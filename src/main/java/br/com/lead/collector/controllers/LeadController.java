package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/leads")
public class LeadController {

    @Autowired
    public LeadService leadServices;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED) //responde o status created
    public Lead cadastrarLead(@RequestBody @Valid Lead lead) {
        Lead retornoLead = leadServices.salvarLead(lead);
        return lead;

        //throw ResponseStatusException //classe de controller
    }

    @GetMapping
    public Iterable<Lead> lerTodosOsLeads() {
        return leadServices.lerTodosOsLeads();
    }

    @GetMapping("/{id}") //entre chaves bigodinho colocar o nome do campo que vai ser usado na queryString da URL
    public Lead pesquisarPorId(@PathVariable(name = "id") int id) {
        try {
            Lead lead = leadServices.buscarLeadPeloId(id);
            return lead;
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PutMapping("/{id}") //obrigado enviar todos os campos para serem atualizados
    public Lead atualizarLead(@RequestBody @Valid Lead lead, @PathVariable(name = "id") int id) {
        try {
            Lead leadDB = leadServices.atualizarLead(id, lead);
            return leadDB;
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deletarLead(@PathVariable(name = "id") int id) {
        try {
            leadServices.deletarLead(id);
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, ex.getMessage());
            //NO_CONTENT -> significa que foi realizado com sucesso, porém não precisa fazer nada com o retorno pois foi feito com sucesso.
        }
    }
}

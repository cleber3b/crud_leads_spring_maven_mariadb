package br.com.lead.collector.services;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.repositories.LeadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class LeadService {

    @Autowired //aqui criamos a injeção de dependencia dop LeadRepository
    private LeadRepository leadRepository;

    public Lead salvarLead(Lead lead){
        Lead objetoLead = leadRepository.save(lead);
        return objetoLead;
    }

    public Iterable<Lead> lerTodosOsLeads(){ //Iterable é mais primitivo, mãe do List
        return leadRepository.findAll();
    }

    public Lead buscarLeadPeloId(int id){
        Optional<Lead> leadOptional = leadRepository.findById(id);

        if(leadOptional.isPresent())
        {
            Lead lead = leadOptional.get();
            return lead;
        }else{
            throw new RuntimeException("O lead não foi encontrado"); //exclusivo erro retorno classe de serviço

        }
    }

    //atualização com PUT (atualiza o Lead inteiro)
    public Lead atualizarLead(int id, Lead lead){
        Lead leadDB = buscarLeadPeloId(id);
        lead.setId(leadDB.getId());
        return leadRepository.save(lead);
    }

    public void deletarLead(int id){

        if(leadRepository.existsById(id)) {
            leadRepository.deleteById(id);
        }else{
            throw new RuntimeException("Registro não existe");
        }
    }
}

package br.com.lead.collector.models;

import org.springframework.format.annotation.NumberFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Produto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull(message = "Nome do Produto não pode ser em Branco")
    @NotBlank(message = "Nome do Produto não pode ser vazio")
    @Size(min = 4, message = "Nome do Produto precisa de no mínimo com 4 caracteres")
    private String nome;
    private String descricao;

    //@Decimal -> já valida entrada Nula, Branco e igual a zero nõa deixando inseriri valor zerado
    @DecimalMin(value = "0", message = "Valor do preço inválido, informe um valor maior que zero(0)", inclusive = false)
    private double preco;

    public Produto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
}

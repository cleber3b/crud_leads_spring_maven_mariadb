package br.com.lead.collector.models;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
//@Table(name = "leads") //tabela que ja existe no banco de dados
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    //@Column(name = "nome123456") //mapeando a coluna que já existe
    @NotNull(message = "Nome não pode ser em Branco")
    @NotBlank(message = "Nome não pode ser vazio")
    @Size(min = 3, message = "Nome mínimo com 3 caracteres")
    private String nome;
    @CPF(message = "CPF inválido")
    @NotNull(message = "Nome não pode ser em Branco")
    private String cpf;
    @Email(message = "Email inválido")
    @NotNull(message = "Nome não pode ser em Branco")
    private String email;
    private String telefone;

    public Lead() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}

package br.com.lead.collector.repositories;

import br.com.lead.collector.models.Lead;
import org.springframework.data.repository.CrudRepository;

public interface LeadRepository extends CrudRepository<Lead, Integer> {
    //extends do Crud Repositorie passando a classe/tabela, o integer do ID tipagem da chave primaria ou da chave composta
    //interface de comunicação com o banco de dados Update, select, insert, delete

}
